'''
FileName: GPUProfiling-FLOPS.py
Author: Viswanathan Kavassery Rajalingam
Date Created: 04/28/2017

Input Parameters: 
1. First Command Line Argument - Pass the Interval time (in Seconds) to Refresh your result.
2. File Path to Log the Output 

Functionalities
1. Prints the Number of GPU's, GPU Utilization, FLOPS and GPU CLOCKs for every interval

TO BE DONE
1. Graphs that dynamically update for the Results.
'''
  
import sys
import subprocess
import time
import os

def parseSMI(values, s):

	noOfCores = 2560
	necessaryValues = (s.strip()).split('\n')
	numberOfGpus = len(necessaryValues)
	#gpuLists = [[] for i in range(numberOfGpus)]
     
	for index, line in enumerate(necessaryValues):
		try:
			elements = [element.strip() for element in line.split(',')]
			gpuUtil = float(elements[0])/100
			smClock = int(elements[1])
			gpuFlop = gpuUtil * 2 * smClock * noOfCores
			gpuFlop = int(gpuFlop)
			#gpuFlop = float(gpuFlop) / 1000000000
			#gpuLists[index].append((gpuUtil,smClock,gpuFlop))
			keyBase = 'GPU'+str(index)
			keyUtil = keyBase+'.Util'
			keyClock = keyBase+'.Clock'
			keyFlop = keyBase+'.Flop'
			if any(k in values.keys() for k in [keyUtil, keyClock, keyFlop]):
				sys.stderr.write('Duplicate Values \n');
			else:
				values[keyUtil] = gpuUtil
	                        values[keyClock] = smClock
	                        values[keyFlop] = gpuFlop
		except Exception as e:
			sys.stderr.write(str(e)+'\n')	
	return numberOfGpus, values

def main(userRate, fileObj):

	rate = 1
	sleepTime = 0.01
	try:
		newRate = (int)(userRate)
	except:
		sys.stderr.write('Invalid Rate, Using Default \n')

	if newRate < 1:
		sys.stderr.write('Rate must be a positive Integer \n')
	else:
		rate = newRate

	startTime = (int)(time.time())+1
	tick = 0
	print "**"*15 +"Custom GPU Profiling" + "**"*15
	fileObj.write("**"*15+"Custom GPU Profiling" + "**"*15 +"\n")
	try:
		while(True):
			nextExecutetime = startTime + (tick * rate)
			currentTime = time.time()

			if currentTime >= nextExecutetime:
				tick = tick + 1
				diff = currentTime - nextExecutetime

				if diff > rate:
					sys.stderr.write('Error: Please Slow Down Your Rate')
				
				numberOfGpus, values = loadLiveData()
				values['timestamp'] = nextExecutetime
				print '--'*15+'TimeStamp: '+str(nextExecutetime)+'--'*15
				fileObj.write("--"*15+'TimeStamp: '+str(nextExecutetime)+'--'*15+"\n")
				print "ACTIVE NUMBER OF GPU's : "+str(numberOfGpus)
				fileObj.write("ACTIVE NUMBER OF GPU's : "+str(numberOfGpus)+"\n")
				printData(values, fileObj)
			time.sleep(sleepTime)

	except KeyboardInterrupt:
		
		print "\nGPU PROFILING SUCCESS"
		fileObj.close()
		sys.exit()

def loadLiveData():
	smi_str = subprocess.check_output(['nvidia-smi','--format=csv,noheader,nounits', '--query-gpu=utilization.gpu,clocks.sm'])
	values = {}
	numberOfGpus, values = parseSMI(values, smi_str)
	return numberOfGpus, values
	
def printData(values,fileObj):
	for i in values.keys():
		if i != 'timestamp':
		
			if 'Util' in i:
				print i, '\t', values[i]*100, ' %'
				fileObj.write(str(i)+"\t"+str(values[i]*100)+' %\n')
			elif 'Clock' in i:
				print i, '\t', values[i], ' MHz'
				fileObj.write(str(i)+"\t"+str(values[i])+" MHz\n")
			else:
				print i, '\t', values[i]
				fileObj.write(str(i)+"\t"+str(values[i])+"\n")
if __name__=='__main__':
	
	if len(sys.argv) > 1:
		userRate = sys.argv[1]
		userFilePath = sys.argv[2]
		try:
			if (os.path.isfile(userFilePath)==False):
				fileObj = open(userFilePath, 'a')
			else:
				raise Exception('File Already Exists!! Please Enter Another FileName\n')
		except Exception as e:
			sys.stderr.write('Error in File Handling\n'+str(e))
			sys.exit()
	else:
		sys.stderr.write('Enter Commandline Arguments \n 1. Time Interval (in Seconds) to Refresh Results \n 2. File Path to Log the Output\n')
		sys.exit()
	main(userRate, fileObj)
	fileObj.close()	
