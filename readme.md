Functionality:
This script helps us to track the number of FLOP's being performed in the GPU.

Requirements:
1. Python 2.7
2. nvidia-smi

Instructions to Run the Script:

1. Start a Terminal Instance.
2. execute python nvidiaGPUProfiling_FLOPs.py [args1] [args2]
   
   where args1 - Time (in Seconds) to refresh your results.
         args2 - Path for a text file to log the output.

   Example:
   python nvidiaGPUProfiling.py 5 ~/FLOPs.txt
